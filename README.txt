After installing the module, visit the admin page to configure your settings:
admin/settings/skin

See the project page for usage instructions:
http://drupal.org/project/skin
