<?php

/**
 * Settings form.
 */
function skin_settings_form() {
  $form['skin_paths'] = array(
    '#title' => t('Skin paths'),
    '#type' => 'textarea',
    '#description' => t("Enter each path you want skinned, one per line. The '*' character is a wildcard. %front is the front page.", array('%front' => '<front>')),
    '#default_value' => variable_get('skin_paths', ''),
  );
  $form['skin_save'] = array(
    '#type' => 'radios',
    '#title' => t('CSS and Javascript'),
    '#description' => t('Optimized CSS and Javascript files are frequently deleted, which could break your skin. You can save an additional copy of these files, so they persist until the next time you build your skin. These files are saved in the "skin" directory within your <a href="@files_url">file system path</a>. You can also adjust your optimization settings on the <a href="@performance_url">performance page</a>.', array('@files_url' => 'admin/settings/file-system', '@performance_url' => url('admin/settings/performance'))),
    '#options' => array(
      0 => "Don't save any files",
      1 => "Save a copy of optimized CSS and JS files",
      2 => "Save a copy of all CSS and JS files",
    ),
    '#default_value' => variable_get('skin_save', 1),
  );
  $form['skin_clear_cache'] = array(
    '#type' => 'checkbox',
    '#title' => t('Clear cached CSS and Javascript skin files.'),
    '#description' => t('Warning: this could cause CSS and Javascript paths to become outdated in your skins.'),
  );
  $form['#submit'][] = 'skin_settings_form_submit';
  return system_settings_form($form);
}

/**
 * Settings form submit.
 */
function skin_settings_form_submit($form, &$form_state) {
  if ($form_state['values']['skin_clear_cache']) {
    _skin_recursive_delete(file_create_path('skin'));
  }
}

/**
 * Recursive delete function.
 */
function _skin_recursive_delete($path) {
  if (is_file($path) or is_link($path)) {
    unlink($path);
  }
  else if (is_dir($path)) {
    $d = dir($path);
    while (($entry = $d->read()) !== FALSE) {
      if ($entry == '.' or $entry == '..') {
        continue;
      }
      $entry_path = $path .'/'. $entry;
      _skin_recursive_delete($entry_path);
    }
    $d->close();
    rmdir($path);
  }
}
